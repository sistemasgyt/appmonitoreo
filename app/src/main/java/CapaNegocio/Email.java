package CapaNegocio;

public class Email {
    private String codigoCuenta;
    private String email;

    public String getCodigoCuenta() {
        return codigoCuenta;
    }

    public void setCodigoCuenta(String codigoCuenta) {
        this.codigoCuenta = codigoCuenta;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Email{" +
                "codigoCuenta='" + codigoCuenta + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
