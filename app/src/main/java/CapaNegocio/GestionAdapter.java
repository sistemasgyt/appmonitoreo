package CapaNegocio;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sistemas.gytasesores.R;

import java.util.List;



public class GestionAdapter extends RecyclerView.Adapter<GestionViewHolder> {

    private List<Gestion> gestiones = null;
    private Context mContext;
    private int lastPosition = -1;

    public GestionAdapter(Context mContext, List<Gestion> gestiones) {
        this.gestiones = gestiones;
        this.mContext = mContext;
    }

    @Override
    public GestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gestion,
                parent, false);
        return new GestionViewHolder(view);
    }

    /*private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            animation.setDuration(300); //duracion
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        } else if ( position < lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }*/

    @Override
    public void onBindViewHolder(final GestionViewHolder holder, int position) {
        //setAnimation(holder.itemView, position);

        holder.txtDni.setText(gestiones.get(position).getDni());
        holder.txtMotivoNoPago.setText(gestiones.get(position).getMotivoNoPago());
        holder.txtHora.setText(gestiones.get(position).getHoraGestion());
        holder.txtGestion.setText(gestiones.get(position).getDescripcion());

        /*holder.mFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.mFavorite.getColorFilter() != null) {
                    holder.mFavorite.clearColorFilter();
                } else {
                    holder.mFavorite.setColorFilter(ContextCompat.getColor(mContext,
                            R.color.colorOrange));
                }
            }
        });*/

        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,holder.txtGestion.getText().toString(),Toast.LENGTH_LONG).show();
                /*Intent mIntent = new Intent(mContext, DetailActivity.class);
                mIntent.putExtra("sender", holder.mSender.getText().toString());
                mIntent.putExtra("title", holder.mEmailTitle.getText().toString());
                mIntent.putExtra("details", holder.mEmailDetails.getText().toString());
                mIntent.putExtra("time", holder.mEmailTime.getText().toString());
                mIntent.putExtra("icon", holder.mIcon.getText().toString());
                mIntent.putExtra("colorIcon", color);
                mContext.startActivity(mIntent);*/
            }
        });

    }

        @Override
        public int getItemCount() {
            return gestiones.size();
        }
}

class GestionViewHolder extends RecyclerView.ViewHolder {
    TextView txtDni;
    TextView txtMotivoNoPago;
    TextView txtHora;
    TextView txtGestion;

    RelativeLayout mLayout;

    GestionViewHolder(View itemView) {
        super(itemView);

        txtDni = itemView.findViewById(R.id.txtDni);
        txtMotivoNoPago = itemView.findViewById(R.id.txtMotivoNoPago);
        txtHora = itemView.findViewById(R.id.txtHora);
        txtGestion = itemView.findViewById(R.id.txtGestion);

        mLayout = itemView.findViewById(R.id.layout);
    }
}

