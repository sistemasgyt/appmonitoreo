package CapaNegocio;

public class Cliente {
    private int idCliente;
    private String razonSocial;
    private String documento;
    private double deuda;
    private double saldo;
    private int cicloFacturacion;
    private String campania;
    private int idCampania;
    private String nroContrato;
    private String coordenadas;

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public int getIdCampania() {
        return idCampania;
    }

    public void setIdCampania(int idCampania) {
        this.idCampania = idCampania;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }


    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public double getDeuda() {
        return deuda;
    }

    public void setDeuda(double deuda) {
        this.deuda = deuda;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getCicloFacturacion() {
        return cicloFacturacion;
    }

    public void setCicloFacturacion(int cicloFacturacion) {
        this.cicloFacturacion = cicloFacturacion;
    }

    public String getCampania() {
        return campania;
    }

    public void setCampania(String campania) {
        this.campania = campania;
    }
}
