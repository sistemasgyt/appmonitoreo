package CapaNegocio;

import java.util.Date;

public class Gestion {

    private String codigoCuenta;
    private String dni;
    private int idCliente;
    private String descripcion;
    private int idMotivoNoPago;
    private Date fechaGestion;
    private int idCampania;
    private int idPersonal;
    private long tiempo;
    private boolean contactado;
    private String estado;
    private String motivoNoPago;
    private String horaGestion;
    private String nroContrato;

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public boolean isContactado() {
        return contactado;
    }

    public void setContactado(boolean contactado) {
        this.contactado = contactado;
    }

    public String getCodigoCuenta() {
        return codigoCuenta;
    }

    public void setCodigoCuenta(String codigoCuenta) {
        this.codigoCuenta = codigoCuenta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdMotivoNoPago() {
        return idMotivoNoPago;
    }

    public void setIdMotivoNoPago(int idMotivoNoPago) {
        this.idMotivoNoPago = idMotivoNoPago;
    }

    public Date getFechaGestion() {
        return fechaGestion;
    }

    public void setFechaGestion(Date fechaGestion) {
        this.fechaGestion = fechaGestion;
    }

    public int getIdCampania() {
        return idCampania;
    }

    public void setIdCampania(int idCampania) {
        this.idCampania = idCampania;
    }

    public int getIdPersonal() {
        return idPersonal;
    }

    public void setIdPersonal(int idPersonal) {
        this.idPersonal = idPersonal;
    }

    public long getTiempo() {
        return tiempo;
    }

    public void setTiempo(long tiempo) {
        this.tiempo = tiempo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMotivoNoPago() {
        return motivoNoPago;
    }

    public void setMotivoNoPago(String motivoNoPago) {
        this.motivoNoPago = motivoNoPago;
    }

    public String getHoraGestion() {
        return horaGestion;
    }

    public void setHoraGestion(String horaGestion) {
        this.horaGestion = horaGestion;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Override
    public String toString() {
        return "Gestion{" +
                "codigoCuenta='" + codigoCuenta + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", contactado=" + contactado +
                ", estado='" + estado + '\'' +
                '}';
    }
}
