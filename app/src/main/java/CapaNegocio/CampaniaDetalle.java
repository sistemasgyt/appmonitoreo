package CapaNegocio;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class CampaniaDetalle {
    private double monto;
    private String cupon;
    private String nroContrato;
    private Date fechaVencimiento;
    private int cuentasVencidas;

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getCupon() {
        return cupon;
    }

    public void setCupon(String cupon) {
        this.cupon = cupon;
    }

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public int getCuentasVencidas() {
        return cuentasVencidas;
    }

    public void setCuentasVencidas(int cuentasVencidas) {
        this.cuentasVencidas = cuentasVencidas;
    }

    @Override
    public String toString() {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        return monto + "|" + cupon + "|" + nroContrato + "|" + formato.format(fechaVencimiento) + "\n";
        //return cuentasVencidas + " " + monto + "  " + "  " + cupon + "  " + nroContrato + " " + fechaVencimiento + "\n";
        /*return "Cuentas Vencidas : " + cuentasVencidas
                + " / Monto: " + monto
                + " / Cupon: " + cupon
                + " / Contrato: " + nroContrato
                + " / Fecha: " + fechaVencimiento + "\n";*/

    }
}
