package CapaDatos;

import android.content.Context;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import CapaNegocio.Campania;

public class CampaniaDatos {
    private static Connection conexion = null;

    public static Campania obtenerCampaniaVigenteCampo(String CodCampania,Context c) throws SQLException, Exception
    {
        Campania ca = null;
        try
        {
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call sp_ObtenerCampaniaVigenteCampoMovil(?)}");
            //cs.setInt(1, idPersonal);
            cs.setString(1, CodCampania);
            ResultSet rs = cs.executeQuery();

            if (rs.next())
            {
                ca = new Campania();
                ca.setIdCampania(rs.getInt("idCampania"));
                ca.setNomenclatura(rs.getString("Nomenclatura"));
                ca.setNombre(rs.getString("NombreCampania"));
            }
        }
        catch (Exception e)
        {
            ca = null;
        }
        finally
        {
            //rs.close();
            conexion.close();
        }
        return ca;
    }
}
