package CapaDatos;

import android.content.Context;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import CapaNegocio.Cliente;

public class ClienteDatos {
    private static Connection conexion = null;
    //private static ResultSet rs = null;

    /*public static Connection testearConexion(){
        return Conexion.obtenerConexion();
    }*/

    public static Cliente obtenerClienteXContrato(String nroContrato, Context c)
            throws SQLException, Exception
    {
        Cliente cl = null;
        //int idPersonal,
        try
        {
            //CrearReporteConFiltros(ObjPersonal.getIdPersona(),CodCuenta,"codigocuenta");
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call sp_ObtenerDetalleClienteXContratoMovil(?)}");
            cs.setString(1, nroContrato);
            ResultSet rs = cs.executeQuery();

            if (rs.next())
            {
                cl = new Cliente();
                cl.setIdCliente(rs.getInt("idCliente"));
                cl.setRazonSocial(rs.getString("razonSocial"));
                cl.setDocumento(rs.getString("dni"));
                cl.setCampania(rs.getString("campania"));
                cl.setIdCampania(rs.getInt("idCampania"));
                cl.setNroContrato(rs.getString("nroContrato"));
                cl.setCoordenadas(rs.getString("coordenadas"));
            }
        }
        catch (Exception e)
        {
            cl = null;
        }
        finally
        {
            //rs.close();
            conexion.close();
        }
        return cl;
    }
}
