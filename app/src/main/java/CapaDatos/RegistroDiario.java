package CapaDatos;

public class RegistroDiario {
    //private String usuario;
    private String turno;
    private String item;
    private String fechaHora;

    public RegistroDiario(String turno, String item, String fechaHora) {
        //this.usuario = usuario;
        this.turno = turno;
        this.item = item;
        this.fechaHora = fechaHora;
    }

    /*public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }*/

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }
}
