package CapaDatos;

import android.content.Context;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import CapaNegocio.Acuerdo;

public class AcuerdoDatos {
    private static Connection conexion = null;

    public static int registrarAcuerdoPago(Acuerdo acuerdo, Context c) throws SQLException, Exception {
        int val;
        try {
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call sp_InsertarAcuerdoPagoMovil(?,?)}");
            cs.setInt(1, acuerdo.getIdGestion());
            cs.setDate(2, acuerdo.getFechaAcuerdo());
            val = cs.executeUpdate();
        } catch (Exception e) {
            val = 0;
        } finally {
            conexion.close();
        }
        return val;
    }
}
