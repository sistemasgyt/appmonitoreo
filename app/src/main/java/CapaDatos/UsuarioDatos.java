package CapaDatos;

import android.content.Context;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import CapaLogica.Funciones;
import CapaNegocio.Usuario;

public class UsuarioDatos {
    private static Connection conexion = null;

    public static Usuario ValidarUsuario(Usuario usuario, Context c,int filter)
            throws SQLException, Exception
    {
        Usuario user = null;
        CallableStatement cs = null;
        try
        {
            conexion = Conexion.obtenerConexion(c);
            cs = conexion.prepareCall("{call sp_ValidarUsuarioMovil(?,?,?)}");
            cs.setString(1, usuario.getIdUsuario());
            cs.setString(2, usuario.getContrasenia());
            cs.setInt(3,filter);
            ResultSet rs = cs.executeQuery();
            if (rs.next())
            {
                user = new Usuario();
                user.setId(rs.getInt("idUsuario"));
                user.setNombre(rs.getString("nombres"));
                user.setApellidos(rs.getString("apellidos"));
                user.setNombresCompletos(user.getNombre() + " " + user.getApellidos());
                user.setIdRol(rs.getInt("idRol"));
                user.setIdUsuario(rs.getString("usuario").toUpperCase());
            }
        }
        catch (SQLException e)
        {
            Funciones.mostrarMensaje(c,"Error al validar usuario");
            //System.out.println("ERROR VALIDAR USUARIO:" + e.getMessage());
        }
        finally
        {
            cs.close();
            conexion.close();
        }
        return user;
    }

    public static List<Usuario> listarUsuarios(Context c, int filter) throws SQLException, Exception
    {
        List<Usuario> users = null;
        Usuario user = null;
        CallableStatement cs = null;
        try
        {
            conexion = Conexion.obtenerConexion(c);
            cs = conexion.prepareCall("{call sp_ValidarUsuarioMovil(?,?,?)}");
            cs.setString(1, "");
            cs.setString(2, "");
            cs.setInt(3,filter);
            ResultSet rs = cs.executeQuery();
            users = new ArrayList<Usuario>();
            while (rs.next())
            {
                user = new Usuario();
                user.setId(rs.getInt("idUsuario"));
                user.setNombre(rs.getString("nombres"));
                user.setApellidos(rs.getString("apellidos"));
                user.setNombresCompletos(user.getApellidos() + " " + user.getNombre());
                user.setIdRol(rs.getInt("idRol"));
                user.setIdUsuario(rs.getString("usuario").toUpperCase());
                users.add(user);
            }
        }
        catch (SQLException e)
        {
            Funciones.mostrarMensaje(c,"Error al validar usuario");
        }
        finally
        {
            cs.close();
            conexion.close();
        }
        return users;
    }

}
