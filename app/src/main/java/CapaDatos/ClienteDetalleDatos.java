package CapaDatos;

import android.content.Context;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import CapaNegocio.CampaniaDetalle;
import CapaNegocio.Gestion;

public class ClienteDetalleDatos {

    private static Connection conexion = null;

    public static List<CampaniaDetalle> listarDetalleCliente(int idCliente, Context c) throws Exception{
        ResultSet rs = null;
        List<CampaniaDetalle> detalles = null;
        CampaniaDetalle detalle = null;
        try{
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call sp_ListarDeudasPorCliente(?,?)}");
            cs.setInt(1,10);
            cs.setInt(2,idCliente);
            rs = cs.executeQuery();
            detalles = new ArrayList<CampaniaDetalle>();
            while(rs.next()){
                detalle = new CampaniaDetalle();
                detalle.setMonto(rs.getDouble("monto"));
                detalle.setCupon(rs.getString("cupon"));
                detalle.setNroContrato(rs.getString("nroContrato"));
                detalle.setFechaVencimiento(rs.getDate("fechaVencimiento"));
                detalle.setCuentasVencidas(rs.getInt("cuentasVencidas"));
                detalles.add(detalle);
            }
        }catch(Exception e){
            detalles = null;
        }
        return detalles;
    }
}
