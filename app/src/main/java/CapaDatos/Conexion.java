package CapaDatos;

import android.content.Context;
import android.os.StrictMode;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;


public class Conexion {

    public static Connection obtenerConexion(Context c){
        Connection conexion = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection("jdbc:jtds:sqlserver://gyt.bounceme.net:1433;databaseName=quavii_db;","sa","sys$$Mg2019");

        }catch (Exception e){
            Toast.makeText(c,e.getMessage(), Toast.LENGTH_LONG).show();
            //conexion = null;
        }
        return conexion;
    }
}
