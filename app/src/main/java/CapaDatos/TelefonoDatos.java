package CapaDatos;

import android.content.Context;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import CapaNegocio.Telefono;

public class TelefonoDatos {

    private static Connection conexion = null;

    public static int agregarTelefono(Telefono tel, Context c)
            throws SQLException, Exception
    {
        int retorno = 0;
        try
        {
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call sp_InsertarNumeroMovil(?,?)}");
            cs.setString(1, tel.getCliente().getDocumento());
            cs.setString(2, tel.getNroTelefono());
            cs.executeUpdate();
            cs.close();
        }
        catch (Exception e)
        {
            retorno = 0;
        }
        finally
        {
            conexion.close();
        }
        return retorno;
    }
}
