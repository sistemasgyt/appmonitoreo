package CapaDatos;

import android.content.Context;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import CapaNegocio.Direccion;
import CapaNegocio.Email;

public class EmailDatos {

    private static Connection conexion = null;

    public static int agregarEmail(Email e, Context c) throws Exception
    {
        int retorno = 0;
        try
        {
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call CorreoRegistrar_sp(?,?,?,?)}");
            cs.setInt(1, 0);
            cs.setString(2, e.getEmail());
            cs.setString(3, e.getCodigoCuenta());
            cs.setString(4, "I");
            cs.executeUpdate();
            cs.close();
        }
        catch (Exception ex)
        {
            retorno = 0;
        }
        finally
        {
            conexion.close();
        }
        return retorno;
    }
}
