package CapaDatos;

import android.content.Context;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import CapaNegocio.Gestion;

public class GestionDatos {

    private static Connection conexion = null;

    public static int registrarGestion(Gestion gestion,Context c) throws SQLException,Exception {
        int retorno = 0;
        try{
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call sp_InsertarGestionMovilAndroidVigente(?,?,?,?,?,?,?,?)}");
            cs.setInt(1,gestion.getIdPersonal());
            cs.setInt(2,gestion.getIdCliente());
            cs.setInt(3,gestion.getIdCampania());
            cs.setInt(4,gestion.getIdMotivoNoPago());
            cs.setString(5,gestion.getDescripcion());
            cs.setBoolean(6,gestion.isContactado());
            cs.setLong(7,gestion.getTiempo());
            cs.setString(8,gestion.getNroContrato());
            retorno = cs.executeUpdate();
            cs.close();
        }catch(Exception e){
            retorno = 0;
        }finally {
            conexion.close();
        }
        return retorno;
    }

    public static int obtenerGestionReciente(int idPersonal, int idCampania, int idCliente,Context c)
    {
        int idGestion = 0;
        ResultSet rs = null;
        try
        {
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call sp_ObtenerUltimaGestionRegistradaMovil(?,?,?)}");
            cs.setInt(1, idPersonal);
            cs.setInt(2, idCliente);
            cs.setInt(3, idCampania);
            rs = cs.executeQuery();
            if (rs.next()) {
                idGestion = rs.getInt("idGestion");
            }
        }
        catch (Exception e)
        {
            idGestion = 0;
        }
        return idGestion;
    }

    public static List<Gestion> listarGestionesDia(int idPersonal,Context c) throws Exception{
        ResultSet rs = null;
        List<Gestion> gestiones = null;
        Gestion g = null;
        try{
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call sp_ListarGestionesDiaMovil(?)}");
            cs.setInt(1,idPersonal);
            rs = cs.executeQuery();
            gestiones = new ArrayList<Gestion>();
            while(rs.next()){
                g = new Gestion();
                g.setDni(rs.getString("dni"));
                g.setDescripcion(rs.getString("descripcion"));
                boolean isContactado = rs.getBoolean("estado");
                if(isContactado)
                    g.setEstado("Contactado");
                else
                    g.setEstado("No Contactado");

                g.setHoraGestion(rs.getString("hora"));
                g.setMotivoNoPago(rs.getString("motivo"));
                gestiones.add(g);
            }
        }catch(Exception e){
            gestiones = null;
        }
        return gestiones;
    }

    public static Gestion obtenerDetalleGestion(String dni,int idPersonal,Context c){
        ResultSet rs = null;
        Gestion gestion = null;
        try{
            conexion = Conexion.obtenerConexion(c);
            CallableStatement cs = conexion.prepareCall("{call sp_ObtenerGestionMovil(?,?)}");
            cs.setString(1,dni);
            cs.setInt(2,idPersonal);
            rs = cs.executeQuery();

            if(rs.next()){
                gestion = new Gestion();
                gestion.setDni(rs.getString("dni"));
                gestion.setDescripcion(rs.getString("descripcion"));
                gestion.setMotivoNoPago(rs.getString("MotivoNoPago"));
                gestion.setFechaGestion(rs.getDate("fechaGestion"));
                gestion.setHoraGestion(rs.getString("HoraGestion"));
            }
        }catch(Exception e){
            gestion = null;
        }
        return gestion;
    }
}
