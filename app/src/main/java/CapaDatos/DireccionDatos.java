package CapaDatos;

import android.content.Context;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import CapaNegocio.Direccion;

public class DireccionDatos {

    private static Connection conexion = null;

    public static int actualizarDireccion(Direccion dir,Context c) throws SQLException, Exception
    {
      int retorno = 0;
      try
      {
        conexion = Conexion.obtenerConexion(c);
        CallableStatement cs = conexion.prepareCall("{call sp_ActualizarDireccion(?,?)}");
        cs.setString(1, dir.getCliente().getDocumento());
        cs.setString(2, dir.getDireccion());
        cs.executeUpdate();
        cs.close();
      }
      catch (Exception e)
      {
          retorno = 0;
      }
      finally
      {
          conexion.close();
      }
      return retorno;
    }

}
