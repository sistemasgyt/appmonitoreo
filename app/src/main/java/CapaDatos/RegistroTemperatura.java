package CapaDatos;

public class RegistroTemperatura {
    private String fecha;
    private String hora;
    private String usuario;
    private String temperatura;

    public RegistroTemperatura(String usuario,String fecha, String hora,String temperatura) {
        this.fecha = fecha;
        this.hora = hora;
        this.usuario = usuario;
        this.temperatura = temperatura;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }
}
