package CapaLogica;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;

import android.view.View;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;


import CapaNegocio.Usuario;

public class Funciones {

    public static void mostrarMensaje(Context c,String mensaje){
        Toast.makeText(c,mensaje, Toast.LENGTH_LONG).show();
    }

    public static int obtenerAnioActual(){
        int year = Calendar.getInstance().get(Calendar.YEAR);
        return year;
    }

    public static String obtenerMesActual(){
        int mes = Calendar.getInstance().get(Calendar.MONTH) + 1;
        String mesActual = "";
        switch(mes){
            case 1:
                mesActual = "ENERO";
                break;
            case 2:
                mesActual = "FEBRERO";
                break;
            case 3:
                mesActual = "MARZO";
                break;
            case 4:
                mesActual = "ABRIL";
                break;
            case 5:
                mesActual = "MAYO";
                break;
            case 6:
                mesActual = "JUNIO";
                break;
            case 7:
                mesActual = "JULIO";
                break;
            case 8:
                mesActual = "AGOSTO";
                break;
            case 9:
                mesActual = "SETIEMBRE";
                break;
            case 10:
                mesActual = "OCTUBRE";
                break;
            case 11:
                mesActual = "NOVIEMBRE";
                break;
            case 12:
                mesActual = "DICIEMBRE";
                break;

        }
        return mesActual;
    }

    /*public static String obtenerFechaCompleta(){
        SimpleDateFormat hora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return hora.format(Calendar.getInstance().getTime());
    }*/

    public static String obtenerFecha(){
        SimpleDateFormat fecha = new SimpleDateFormat("dd-MM-yyyy");
        return fecha.format(Calendar.getInstance().getTime());
    }

    public static String obtenerHoraActual(){
        SimpleDateFormat hora = new SimpleDateFormat("HH:mm:ss");
        return hora.format(Calendar.getInstance().getTime());
    }

    public static void guardarEstadoSesion(Context c, boolean isActivo, Usuario usuario)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("isActivo",isActivo);
        editor.putInt("idUsuario",usuario.getId());
        editor.putString("usuario",usuario.getIdUsuario());
        editor.putString("nombres",usuario.getNombre() + " " + usuario.getApellidos());
        editor.putInt("idRol",usuario.getIdRol());
        editor.apply();
    }

    public static boolean obtenerEstadoSesion(Context c)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getBoolean("isActivo",false);
    }

    public static String obtenerUsuarioActivo(Context c)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getString("usuario","NONE");
    }

    public static int obtenerRolUsuarioActivo(Context c)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getInt("idRol",0);
    }

    public static String obtenerDatosUsuario(Context c) //obtiene nombres y apellidos
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getString("nombres","ANONYMOUS");
    }

    public static int obtenerIdentificadorUsuario(Context c)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getInt("idUsuario",0);
    }

    public static void mostrarMensajeSnack(View view, String mensaje)
    {
        Snackbar.make(view,mensaje,Snackbar.LENGTH_LONG).show();
    }

    //funcion para determinar si es mañana o tarde
    public static String obtenerTurnoActual(){
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        String turno = "";

        if(timeOfDay >= 0 && timeOfDay < 12){
            turno = "M";
        }else if(timeOfDay >= 12 && timeOfDay < 19){
            turno = "T";
        }else if(timeOfDay >= 19 && timeOfDay <= 23){
            turno = "N";
        }else{
            turno = "U";//undefined
        }
        return turno;
    }



    //esta funcion se llama al iniciar el activity
    public static void iniciarRegistroTurnos(Context c,String fecha){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fecha",fecha);
        editor.putBoolean("tm",false);
        editor.putBoolean("tt",false);
        editor.apply();
    }

    //funcion para obtener la fecha del ultimo registro
    public static String obtenerUltimaFechaRegistro(Context c)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getString("fecha","01-01-2000");
    }

    //funcion para obtener el registro por turno
    public static Boolean obtenerRegistroTurno(Context c,String turno){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        if(turno.equals("M"))
            return preferences.getBoolean("tm",false);
        else if(turno.equals("T"))
            return preferences.getBoolean("tt",false);
        else
            return false;

    }

    //funcion para actualizar el registro por turno
    public static void actualizarRegistroTurno(Context c,String turno){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = preferences.edit();

        if(turno.equals("M"))
            editor.putBoolean("tm",true);
        else if(turno.equals("T"))
            editor.putBoolean("tt",true);
        else{
            editor.putBoolean("tm",false);
            editor.putBoolean("tt",false);
        }

        editor.apply();
    }

}
