package CapaLogica;

import com.sistemas.gytasesores.LoginActivity;
import com.sistemas.gytasesores.MainActivity;

public class Constantes {

    //CANALES NOTIFICACIONES ANDROID 8 DEL SISTEMA
    public static final String CHANNEL_ID = "GT MONITOREO";
    public static final String CHANNEL_NAME = "GT NOTIFICACIONES";
    public static final String CHANNEL_DESC= "GTAsesores Notificaciones";

    //MAIN ACTIVITY
    public static MainActivity MAIN = null;

}
