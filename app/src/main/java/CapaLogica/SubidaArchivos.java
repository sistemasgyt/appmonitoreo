package CapaLogica;

import android.content.Context;
import java.io.File;

public class SubidaArchivos {

    /*public static void buscarCarpetaTrabajoCampo(Context c,DriveServiceHelper mDriveService,
                                                 String path,String usuario){
        if(mDriveService != null){
            mDriveService.queryFolders("TRABAJO_CAMPO")
                    .addOnSuccessListener(fileList -> {
                        if(fileList.getFiles().size() > 0){
                            buscarCarpetaFotosCampo(c,mDriveService,path,usuario);
                            //Funciones.mostrarMensaje(c,"La carpeta TRABAJO DE CAMPO existe");
                        }else{
                            Funciones.mostrarMensaje(c,"La carpeta TRABAJO DE CAMPO no existe ");
                        }
                    })
                    .addOnFailureListener(exception -> {
                        Funciones.mostrarMensaje(c,"Se produjo un error:" + exception.getMessage());
                    });
        }else{
            Funciones.mostrarMensaje(c,"Api Drive no está instanciado");
        }
    }*/


    /*private static void buscarCarpetaFotosCampo(Context c,DriveServiceHelper mDriveService,
                                                String path,String usuario){
        if(mDriveService != null){
            mDriveService.queryFolders("FOTOS " + Funciones.obtenerAnioActual())
                    .addOnSuccessListener(fileList -> {
                        if(fileList.getFiles().size() > 0){
                            buscarCarpetaMesFotos(c,mDriveService,path,usuario);
                            //Funciones.mostrarMensaje(c,"La carpeta FOTOS " + Funciones.obtenerAnioActual() + " existe");
                        }else{
                            Funciones.mostrarMensaje(c,"La carpeta FOTOS " + Funciones.obtenerAnioActual() +  " no existe ");
                        }
                    })
                    .addOnFailureListener(exception -> {
                        Funciones.mostrarMensaje(c,"Se produjo un error:" + exception.getMessage());
                    });
        }else{
            Funciones.mostrarMensaje(c,"Api Drive no está instanciado");
        }
    }*/

    /*private static void buscarCarpetaMesFotos(Context c,DriveServiceHelper mDriveService,
                                              String path,String usuario){
        if(mDriveService != null){
            mDriveService.queryFolders("TCF_" + Funciones.obtenerMesActual())
                    .addOnSuccessListener(fileList -> {
                        if(fileList.getFiles().size() > 0){
                            buscarCarpetaUsuario(c,mDriveService,path,usuario);
                            //Funciones.mostrarMensaje(c,"La carpeta TCF_" + Funciones.obtenerMesActual() +  " existe ");
                        }else{
                            Funciones.mostrarMensaje(c,"La carpeta TCF_" + Funciones.obtenerMesActual() +  " no existe");
                        }
                    })
                    .addOnFailureListener(exception -> {
                        Funciones.mostrarMensaje(c,"Se produjo un error:" + exception.getMessage());
                    });
        }else{
            Funciones.mostrarMensaje(c,"Api Drive no está instanciado");
        }
    }*/

    /*private static void buscarCarpetaUsuario(Context c,DriveServiceHelper mDriveService,
                                             String path,String usuario){
        if(mDriveService != null){
            mDriveService.queryFolders("TCF_" + usuario)
                    .addOnSuccessListener(fileList -> {
                        if(fileList.getFiles().size() > 0){
                            //Funciones.mostrarMensaje(c,"La carpeta TCF_" + usuario + " existe");
                            crearSubCarpetaDia(c,Funciones.obtenerFechaActual(),mDriveService,
                                    fileList.getFiles().get(0).getId(),path,usuario);
                        }else{
                            Funciones.mostrarMensaje(c,"La carpeta TCF_" + usuario +  " no existe ");
                        }
                    })
                    .addOnFailureListener(exception -> {
                        Funciones.mostrarMensaje(c,"Se produjo un error:" + exception.getMessage());
                    });
        }else{
            Funciones.mostrarMensaje(c,"Api Drive no está instanciado");
        }
    }*/

    /*private static void crearSubCarpetaDia(Context c,String carpeta,
                                        DriveServiceHelper mDriveService,String idFolder,
                                        String path,String usuario){
        if(mDriveService != null){
            mDriveService.queryFolders(usuario+"_"+carpeta)
                    .addOnSuccessListener(fileList -> {
                        if(fileList.getFiles().size() > 0){
                            //Funciones.mostrarMensaje(c,"La carpeta " + carpeta +  " existe ");
                        }else{
                            //Funciones.mostrarMensaje(c,"La carpeta " + carpeta +  " no existe");
                            crearCarpeta(c,usuario+"_"+carpeta,
                                    mDriveService,idFolder);
                        }
                        subirArchivoEnCarpeta(c,carpeta,mDriveService,path,usuario);
                    })
                    .addOnFailureListener(exception -> {
                        Funciones.mostrarMensaje(c,"Se produjo un error:" + exception.getMessage());
            });
        }else{
            Funciones.mostrarMensaje(c,"Api Drive no está instanciado");
        }
    }*/

    /*private static void crearCarpeta(Context c,String carpeta,
                                        DriveServiceHelper mDriveService,String idFolder){
        if(mDriveService != null){
            mDriveService.createSubFolder(carpeta,idFolder)
                    .addOnSuccessListener(fileId -> {
                        Funciones.mostrarMensaje(c,"Carpeta Creada: " + carpeta + " con ID: " + fileId);
                    })
                    .addOnFailureListener(exception -> {
                        Funciones.mostrarMensaje(c,"Se produjo un error:" + exception.getMessage());
                    });
        }else{
            Funciones.mostrarMensaje(c,"Api Drive no está instanciado");
        }
    }*/

    /*public static void subirArchivoEnCarpeta(Context c, String carpeta,
                                             DriveServiceHelper mDriveService,
                                             String dir,String usuario) {
            if(mDriveService != null){
            mDriveService.obtenerIdCarpeta(usuario+"_"+carpeta)
                    .addOnSuccessListener(fileList -> {
                        File directory = new File(dir + "/GESTIONES_CAMPO_FOTOS/"+usuario.toUpperCase()+"/"+carpeta);
                        File[] files = directory.listFiles();
                        try {
                            Thread.sleep(1);
                            subirArchivo(c,fileList.getFiles().get(0).getId(),mDriveService,
                                    directory.getAbsolutePath() + "/" +files[files.length - 1].getName(),files[files.length - 1].getName());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    })
                    .addOnFailureListener(exception -> {
                        Funciones.mostrarMensaje(c,"Se produjo un error al obtener ID: " + exception.getMessage());
                    });
        }else{
            Funciones.mostrarMensaje(c,"Api Drive no está instanciado");
        }
    }*/

    /*private static void subirArchivo(Context c,String idCarpetaDrive,
                                     DriveServiceHelper mDriveService,String pathCadena,
                                     String archivo){
        if(mDriveService != null){
            mDriveService.uploadFileToFolder(idCarpetaDrive,c,pathCadena,archivo)
                    .addOnSuccessListener(fileId -> {
                        Funciones.mostrarMensaje(c,"Archivo subido con ID: " + fileId);
                    })
                    .addOnFailureListener(exception -> {
                        Funciones.mostrarMensaje(c,"Se produjo un error al subir archivo" + exception.getMessage());
                    });
        }else{
            Funciones.mostrarMensaje(c,"Api Drive no está instanciado");
        }
    }*/
}
