package com.sistemas.gytasesores;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;



import CapaDatos.UsuarioDatos;
import CapaLogica.Constantes;
import CapaLogica.Funciones;
import CapaNegocio.Usuario;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class LoginActivity extends AppCompatActivity {

    EditText txtUsuario;
    EditText txtContrasenia;
    Button btnAcceder;
    Usuario usuario = null;

    private static final int REQUEST_PERMISSION = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //veririfica si el usuario esta logueado, si lo esta, la aplicacion redirige al main
        if(Funciones.obtenerEstadoSesion(getApplicationContext()))
        {
            openMainActivity();
        }

        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtContrasenia = (EditText) findViewById(R.id.txtContrasenia);
        btnAcceder = (Button) findViewById(R.id.btnAcceder);
        usuario = new Usuario();

        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = null;
                usuario.setIdUsuario(txtUsuario.getText().toString());
                usuario.setContrasenia(txtContrasenia.getText().toString());

                try {
                    if(usuario.getIdUsuario() == "RRHH"){
                        Usuario u = new Usuario();
                        u.setIdUsuario("RRHH");
                        u.setNombre("RECURSOS");
                        u.setApellidos("HUMANOS");
                        u.setNombresCompletos("RECURSOS HUMANOS");
                        u.setIdRol(1002);
                        u.setId(1);

                        if(u.getIdRol() == 1002){//si es rrhh
                            intent = new Intent(getApplicationContext(), TemperatureActivity.class);
                        }else{
                            intent = new Intent(getApplicationContext(), MonitorActivity.class);
                        }

                        intent.putExtra("objUsuario",u);
                        //guardar en en shared preferences el usuario logueado
                        Funciones.guardarEstadoSesion(getApplicationContext(),true,u);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            finish();
                            startActivity(intent);
                        }
                    }
                    /*Usuario u = UsuarioDatos.ValidarUsuario(usuario,getApplicationContext(),1);
                    if(u == null){
                        Funciones.mostrarMensaje(getApplicationContext(),"Usuario/Contraseña Invalidos");
                    }else{
                        Funciones.mostrarMensaje(getApplicationContext(),"Credenciales Correctas");

                        if(u.getIdRol() == 1002){//si es rrhh
                            intent = new Intent(getApplicationContext(), TemperatureActivity.class);
                        }else{
                            intent = new Intent(getApplicationContext(), MonitorActivity.class);
                        }

                        intent.putExtra("objUsuario",u);
                        //guardar en en shared preferences el usuario logueado
                        Funciones.guardarEstadoSesion(getApplicationContext(),true,u);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            finish();
                            startActivity(intent);
                        }
                    }*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

       /* //Check whether GPS tracking is enabled//
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            finish();
        }*/

        //CREA CANAL NOTIFICACIONES GENERAL
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel(Constantes.CHANNEL_ID,Constantes.CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription(Constantes.CHANNEL_DESC);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        /*int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        //If the location permission has been granted, then start the TrackerService//
        if (permission == PackageManager.PERMISSION_GRANTED) {
            startTrackerService();
        } else {//If the app doesn’t currently have access to the user’s location, then request access//
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);
        }*/

        checkPermission();

    }

    private void openMainActivity(){
        int idRolActivo = Funciones.obtenerRolUsuarioActivo(getApplicationContext());
        Intent intent = null;
        if(idRolActivo == 1002){
            intent = new Intent(getApplicationContext(), TemperatureActivity.class);
        }else{
            intent = new Intent(getApplicationContext(), MonitorActivity.class);
        }

        intent.putExtra("objUsuario",
                new Usuario(Funciones.obtenerUsuarioActivo(getApplicationContext()),
                        Funciones.obtenerDatosUsuario(getApplicationContext()),
                        Funciones.obtenerIdentificadorUsuario(getApplicationContext())));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finish();
            startActivity(intent);
        }

    }

    public boolean checkPermission() {
        boolean retorno = false;
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, INTERNET) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(LoginActivity.this, CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(LoginActivity.this, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(LoginActivity.this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(LoginActivity.this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                retorno = true;
                //startTrackerService();
        }
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, INTERNET) == PackageManager.PERMISSION_DENIED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, CAMERA) == PackageManager.PERMISSION_DENIED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) LoginActivity.this, Manifest.permission.INTERNET) ||
                    ActivityCompat.shouldShowRequestPermissionRationale((Activity) LoginActivity.this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale((Activity) LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale((Activity) LoginActivity.this, ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale((Activity) LoginActivity.this, READ_EXTERNAL_STORAGE)) {
                requestPermission();
            }else{
                requestPermission();
            }
        }
        return retorno;
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions((Activity) this, new String[]{INTERNET,CAMERA,WRITE_EXTERNAL_STORAGE,ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);
    }


}
