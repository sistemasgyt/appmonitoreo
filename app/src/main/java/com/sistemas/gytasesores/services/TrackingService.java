package com.sistemas.gytasesores.services;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

/*import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;*/
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.sistemas.gytasesores.MainActivity;
import com.sistemas.gytasesores.R;


import CapaLogica.Constantes;
/*import CapaLogica.Funciones;
import CapaNegocio.Ubicacion;*/

public class TrackingService extends Service {

    @Override
    public void onCreate() {
        buildNotification();
        //super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        buildNotification();
        checkStatusFirebase();

        Log.d("GPS TRACK","Servicio GPS TRACK Ejecutandose");
        //return super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    private void buildNotification() {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notificationIntent,0);
            Notification notification = new Notification.Builder(this, Constantes.CHANNEL_ID)
                    .setContentTitle("Tracking Trabajo Campo")
                    .setContentText("Servicio Iniciado")
                    .setOngoing(true)
                    .setSmallIcon(R.drawable.ic_gps_fixed_black_24dp)
                    .setContentIntent(pendingIntent)
                    .build();

            startForeground(1,notification);
        }


    }

    private void checkStatusFirebase()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (task.isSuccessful()){
                    //requestLocationUpdates();
                }else{
                    Toast.makeText(getApplicationContext(),"Error: " + task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /*private void requestLocationUpdates() {
        LocationRequest request = new LocationRequest();
        //Specify how often your app should request the device’s location
        request.setInterval(30000);
        //Get the most accurate location data available
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        final String path = getString(R.string.firebase_path);

        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        //If the app currently has access to the location permission
        if (permission == PackageManager.PERMISSION_GRANTED) {
            client.requestLocationUpdates(request, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    if(!Funciones.obtenerUsuarioActivo(Constantes.MAIN).equals("NONE")){
                        //Get a reference to the database, so your app can perform read and write operations
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(path);
                        Location location = locationResult.getLastLocation();
                        if (location != null) {
                            Ubicacion ubicacion = new Ubicacion(location.getLatitude(),location.getLongitude(), Funciones.obtenerFecha(),Funciones.obtenerHoraActual());

                            DatabaseReference newRef = ref.child(Funciones.obtenerUsuarioActivo(Constantes.MAIN).toUpperCase()).child(String.valueOf(Funciones.obtenerAnioActual())).child(Funciones.obtenerFecha()).push();
                            //Save the location data to the database
                            newRef.setValue(ubicacion);
                        }
                    }

                }
            }, null);
        }
    }*/

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
