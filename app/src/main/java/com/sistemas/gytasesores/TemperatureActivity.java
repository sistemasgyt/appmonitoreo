package com.sistemas.gytasesores;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import CapaDatos.RegistroTemperatura;
import CapaDatos.UsuarioDatos;
import CapaLogica.Funciones;
import CapaNegocio.Usuario;

public class TemperatureActivity extends AppCompatActivity {

    public static final int CAMERA_REQUEST_CODE = 100;
    public static final int RESULT_OK = -1;

    String MEDIA_DIRECTORY,FILE_NAME;
    File photo;
    String currentPhotoPath;

    Spinner cbUsuarios;
    //ImageView imgPhoto;
    Button btnCamara;
    Button btnRegistrar;
    EditText txtTemperatura;

    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);

        cbUsuarios = (Spinner) findViewById(R.id.cbUsuarios);
        //imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
        btnCamara = (Button)findViewById(R.id.btnCamara);
        btnRegistrar = (Button)findViewById(R.id.btnRegistrar);
        txtTemperatura = (EditText)findViewById(R.id.txtTemperatura);

        //inicializando referencia al storage en firebase
        storageReference = FirebaseStorage.getInstance().getReference("photos-temperature");
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        List<String> items = new ArrayList<String>();
        items.add("SELECCIONE USUARIO");

        try{
            List<Usuario> users = UsuarioDatos.listarUsuarios(getApplicationContext(),0);

            for (Usuario u: users){
                items.add(u.getNombresCompletos().toUpperCase());
            }

        }catch (Exception e){
            Log.d("TAG",e.getMessage());
        }

        ArrayAdapter<String> adp3 =
                new ArrayAdapter<>(getApplicationContext(),
                        android.R.layout.simple_spinner_item,items);
        adp3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbUsuarios.setAdapter(adp3);

        //evento al boton camara
        btnCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String usuario = cbUsuarios.getSelectedItem().toString();
                    if(!usuario.equals("SELECCIONE USUARIO"))
                        abrirCamara(usuario,CAMERA_REQUEST_CODE);
                    else
                        Funciones.mostrarMensaje(getApplicationContext(),"No ha seleccionado un usuario");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //evento al boton registrar
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    saveDataToFirebase();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void abrirCamara(String item,int code){

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(item);
            } catch (IOException ex) {
                Log.e("Error",ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, code);
            }
        }

    }

    private File createImageFile(String item) throws IOException {
        //trabajando con fechas
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        //definiendo directorio
        MEDIA_DIRECTORY = "TEMPERATURE/" + dateFormat.format(date);
        //definiendo nombre del archivo
        FILE_NAME = item + "_" + dateFormat.format(date) + "_" + Funciones.obtenerHoraActual()  +".png";

        // Create an image file name
        File storageDir = new File(Environment.getExternalStorageDirectory(),MEDIA_DIRECTORY);

        if(!storageDir.exists())
            storageDir.mkdirs();

        photo = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY + "/" + FILE_NAME);

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = photo.getAbsolutePath();
        return photo;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK)  {
            File f = new File(currentPhotoPath);
            Uri contentUri = Uri.fromFile(f);

           /* switch (requestCode){
                case CAMERA_REQUEST_CODE:
                    imgPhoto.setImageURI(contentUri);
                    break;
            }

            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);*/

            //metodo que sube la imagen a firestorage
            uploadImageToFirebase(f.getName(),contentUri);
        }
    }


    private void uploadImageToFirebase(String name, Uri contentUri) {
        StorageReference image = storageReference.child(Funciones.obtenerFecha() + "/" + name);
        image.putFile(contentUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Funciones.mostrarMensaje(getApplicationContext(),"Imagen Subida Correctamente");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Funciones.mostrarMensaje(getApplicationContext(),"Ha ocurrido un error al cargar la imagen");
            }
        });
    }

    private void saveDataToFirebase(){
        //obtengo datos para enviarlo al constructor
        String fecha = Funciones.obtenerFecha();
        String hora = Funciones.obtenerHoraActual();
        String usuario = cbUsuarios.getSelectedItem().toString();
        String temperatura = txtTemperatura.getText().toString();

        if(usuario.equals("SELECCIONE USUARIO") || temperatura.equals("")){
            Funciones.mostrarMensaje(getApplicationContext(),"Hay datos faltantes por ingresar");
        }else{
            //creacion del objeto registro
            RegistroTemperatura registro = new RegistroTemperatura(usuario,fecha,hora,temperatura);

            //referencia a la raiz de mi bd
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("bdtemperature");

            //estructura de coleccion
            DatabaseReference newRef = ref.child(Funciones.obtenerFecha()).push();

            //guarda registro en bd de firebase
            newRef.setValue(registro).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Funciones.mostrarMensaje(getApplicationContext(),"Registrado Correctamente");
                    clearFields();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("TG","Ocurrio un error al guardar la data");
                }
            });
        }
    }

    private void clearFields() {
        txtTemperatura.setText("");
        //imgPhoto.setImageResource(0);
        cbUsuarios.setSelection(0);
    }
}