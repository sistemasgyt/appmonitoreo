package com.sistemas.gytasesores;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Intent;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import CapaDatos.RegistroDiario;
import CapaLogica.Funciones;
import CapaNegocio.Usuario;

public class CheckActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int CAMERA_REQUEST_CODE_PRIMER_ITEM = 100;
    public static final int CAMERA_REQUEST_CODE_SEGUNDO_ITEM = 101;
    public static final int CAMERA_REQUEST_CODE_TERCER_ITEM = 102;
    public static final int CAMERA_REQUEST_CODE_CUARTO_ITEM = 103;
    public static final int CAMERA_REQUEST_CODE_QUINTO_ITEM = 104;

    public static final int RESULT_OK = -1;

    File photo;

    String MEDIA_DIRECTORY,FILE_NAME;
    String usuarioActivo = "ME";
    String currentPhotoPath;

    RelativeLayout checkLayout;

    //Primer Item
    RelativeLayout rvPrimerItem;
    TextView lblPrimerItem;
    ImageView imgPhotoPrimerItem;
    Button btnSiguientePrimerItem;
    Button btnCamaraPrimerItem;

    //Segundo Item
    RelativeLayout rvSegundoItem;
    TextView lblSegundoItem;
    ImageView imgPhotoSegundoItem;
    Button btnSiguienteSegundoItem;
    Button btnCamaraSegundoItem;

    //Tercer Item
    RelativeLayout rvTercerItem;
    TextView lblTercerItem;
    ImageView imgPhotoTercerItem;
    Button btnSiguienteTercerItem;
    Button btnCamaraTercerItem;

    //Cuarto Item
    RelativeLayout rvCuartoItem;
    TextView lblCuartoItem;
    ImageView imgPhotoCuartoItem;
    Button btnSiguienteCuartoItem;
    Button btnCamaraCuartoItem;

    //Quinto Item
    RelativeLayout rvQuintoItem;
    TextView lblQuintoItem;
    ImageView imgPhotoQuintoItem;
    Button btnCamaraQuintoItem;
    Button btnFinalizar;

    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);

        //inicializando referencia al storage en firebase
        storageReference = FirebaseStorage.getInstance().getReference("photos");

        checkLayout = (RelativeLayout) findViewById(R.id.checkLayout);

        //asignando variables primerItem
        lblPrimerItem = (TextView) findViewById(R.id.lblPrimerItem);
        imgPhotoPrimerItem = (ImageView) findViewById(R.id.imgPhotoPrimerItem);
        btnSiguientePrimerItem = (Button)findViewById(R.id.btnSiguientePrimerItem);
        btnSiguientePrimerItem.setOnClickListener(this);
        btnCamaraPrimerItem = (Button)findViewById(R.id.btnCamaraPrimerItem);
        btnCamaraPrimerItem.setOnClickListener(this);
        rvPrimerItem = (RelativeLayout) findViewById(R.id.rvPrimerItem);//primera vista

        //asignando variables segundoItem
        lblSegundoItem = (TextView) findViewById(R.id.lblSegundoItem);
        imgPhotoSegundoItem = (ImageView) findViewById(R.id.imgPhotoSegundoItem);
        btnSiguienteSegundoItem = (Button)findViewById(R.id.btnSiguienteSegundoItem);
        btnSiguienteSegundoItem.setOnClickListener(this);
        btnCamaraSegundoItem = (Button)findViewById(R.id.btnCamaraSegundoItem);
        btnCamaraSegundoItem.setOnClickListener(this);
        rvSegundoItem = (RelativeLayout) findViewById(R.id.rvSegundoItem);//segunda vista

        //asignando variables tercerItem
        lblTercerItem = (TextView) findViewById(R.id.lblTercerItem);
        imgPhotoTercerItem = (ImageView) findViewById(R.id.imgPhotoTercerItem);
        btnSiguienteTercerItem = (Button)findViewById(R.id.btnSiguienteTercerItem);
        btnSiguienteTercerItem.setOnClickListener(this);
        btnCamaraTercerItem = (Button)findViewById(R.id.btnCamaraTercerItem);
        btnCamaraTercerItem.setOnClickListener(this);
        rvTercerItem = (RelativeLayout) findViewById(R.id.rvTercerItem);//tercera vista

        //asignando variables cuartoItem
        lblCuartoItem = (TextView) findViewById(R.id.lblCuartoItem);
        imgPhotoCuartoItem = (ImageView) findViewById(R.id.imgPhotoCuartoItem);
        btnSiguienteCuartoItem = (Button)findViewById(R.id.btnSiguienteCuartoItem);
        btnSiguienteCuartoItem.setOnClickListener(this);
        btnCamaraCuartoItem = (Button)findViewById(R.id.btnCamaraCuartoItem);
        btnCamaraCuartoItem.setOnClickListener(this);
        rvCuartoItem = (RelativeLayout) findViewById(R.id.rvCuartoItem);//cuarta vista

        //asignando variables quintoItem
        lblQuintoItem = (TextView) findViewById(R.id.lblQuintoItem);
        imgPhotoQuintoItem = (ImageView) findViewById(R.id.imgPhotoQuintoItem);
        btnFinalizar = (Button)findViewById(R.id.btnFinalizar);
        btnCamaraQuintoItem = (Button)findViewById(R.id.btnCamaraQuintoItem);
        btnCamaraQuintoItem.setOnClickListener(this);
        rvQuintoItem = (RelativeLayout) findViewById(R.id.rvQuintoItem);//quinto vista

        //ocultando todos los botones con id #siguiente y finalizar
        btnSiguientePrimerItem.setVisibility(View.GONE);
        btnSiguienteSegundoItem.setVisibility(View.GONE);
        btnSiguienteTercerItem.setVisibility(View.GONE);
        btnSiguienteCuartoItem.setVisibility(View.GONE);
        btnFinalizar.setVisibility(View.GONE);

        //evento al foton finalizar - ultimo item
        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //actualizo los datos del registro segun turno actual
                    String turnoActual = Funciones.obtenerTurnoActual();
                    Funciones.actualizarRegistroTurno(getApplicationContext(),turnoActual);

                    //guardar item en la bd
                    saveDataToFirebase(getString(R.string.quinto_item_clase));

                    Thread.sleep(1500);

                    //dirigir a la ventana principal
                    Intent intent = new Intent(getApplicationContext(), MonitorActivity.class);
                    intent.putExtra("objUsuario",
                            new Usuario(Funciones.obtenerUsuarioActivo(getApplicationContext()),
                                    Funciones.obtenerDatosUsuario(getApplicationContext()),
                                    Funciones.obtenerIdentificadorUsuario(getApplicationContext())));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        finish();
                        startActivity(intent);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        //se evalua los botones de la camara y los botones siguiente
        if(id == R.id.btnCamaraPrimerItem){
            abrirCamara(getString(R.string.primer_item_clase),CAMERA_REQUEST_CODE_PRIMER_ITEM);
        }else if(id == R.id.btnCamaraSegundoItem){
            abrirCamara(getString(R.string.segundo_item_clase),CAMERA_REQUEST_CODE_SEGUNDO_ITEM);
        }else if(id == R.id.btnCamaraTercerItem){
            abrirCamara(getString(R.string.tercer_item_clase),CAMERA_REQUEST_CODE_TERCER_ITEM);
        }else if(id == R.id.btnCamaraCuartoItem){
            abrirCamara(getString(R.string.cuarto_item_clase),CAMERA_REQUEST_CODE_CUARTO_ITEM);
        }else if(id == R.id.btnCamaraQuintoItem){
            abrirCamara(getString(R.string.quinto_item_clase),CAMERA_REQUEST_CODE_QUINTO_ITEM);
        }else if(id == R.id.btnSiguientePrimerItem){
            try {
                rvPrimerItem.setVisibility(View.GONE);//oculta el contenedor del primer item
                Thread.sleep(1000);
                rvSegundoItem.setVisibility(View.VISIBLE);//muestra el contenedor del segundo item

                //guardar item en la bd
                saveDataToFirebase(getString(R.string.primer_item_clase));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else if(id == R.id.btnSiguienteSegundoItem){
            try {
                rvSegundoItem.setVisibility(View.GONE);
                Thread.sleep(1000);
                rvTercerItem.setVisibility(View.VISIBLE);

                //guardar item en la bd
                saveDataToFirebase(getString(R.string.segundo_item_clase));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else if(id == R.id.btnSiguienteTercerItem){
            try {
                rvTercerItem.setVisibility(View.GONE);
                Thread.sleep(1000);
                rvCuartoItem.setVisibility(View.VISIBLE);

                //guardar item en la bd
                saveDataToFirebase(getString(R.string.tercer_item_clase));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else if(id == R.id.btnSiguienteCuartoItem){
            try {
                rvCuartoItem.setVisibility(View.GONE);
                Thread.sleep(1000);
                rvQuintoItem.setVisibility(View.VISIBLE);

                //guardar item en la bd
                saveDataToFirebase(getString(R.string.cuarto_item_clase));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private void abrirCamara(String item,int code){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(item);
            } catch (IOException ex) {
                Log.e("Error",ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, code);
            }
        }
    }

    private File createImageFile(String item) throws IOException {
        //trabajando con fechas
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        //definiendo directorio
        MEDIA_DIRECTORY = "REGISTRO_LIMPIEZA/" + usuarioActivo.toUpperCase() + "/" + dateFormat.format(date);
        //definiendo nombre del archivo
        FILE_NAME = item + "_" + dateFormat.format(date) + "_" + Funciones.obtenerHoraActual()  +".png";

        // Create an image file name
        File storageDir = new File(Environment.getExternalStorageDirectory(),MEDIA_DIRECTORY);

        if(!storageDir.exists())
            storageDir.mkdirs();

        photo = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY + "/" + FILE_NAME);

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = photo.getAbsolutePath();
        return photo;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK)  {
            File f = new File(currentPhotoPath);
            Uri contentUri = Uri.fromFile(f);

            switch (requestCode){
                case CAMERA_REQUEST_CODE_PRIMER_ITEM:
                    imgPhotoPrimerItem.setImageURI(contentUri);
                    btnSiguientePrimerItem.setVisibility(View.VISIBLE);
                    break;
                case CAMERA_REQUEST_CODE_SEGUNDO_ITEM:
                    imgPhotoSegundoItem.setImageURI(contentUri);
                    btnSiguienteSegundoItem.setVisibility(View.VISIBLE);
                    break;
                case CAMERA_REQUEST_CODE_TERCER_ITEM:
                    imgPhotoTercerItem.setImageURI(contentUri);
                    btnSiguienteTercerItem.setVisibility(View.VISIBLE);
                    break;
                case CAMERA_REQUEST_CODE_CUARTO_ITEM:
                    imgPhotoCuartoItem.setImageURI(contentUri);
                    btnSiguienteCuartoItem.setVisibility(View.VISIBLE);
                    break;
                case CAMERA_REQUEST_CODE_QUINTO_ITEM:
                    imgPhotoQuintoItem.setImageURI(contentUri);
                    btnFinalizar.setVisibility(View.VISIBLE);
                    break;
            }

            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

            //metodo que sube la imagen a firestorage
            uploadImageToFirebase(f.getName(),contentUri);
        }
    }


    private void uploadImageToFirebase(String name, Uri contentUri) {
        String path = Funciones.obtenerUsuarioActivo(this).toUpperCase() + "/" + Funciones.obtenerFecha();
        StorageReference image = storageReference.child(path + "/" + name);
        image.putFile(contentUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Funciones.mostrarMensaje(getApplicationContext(),"Imagen Subida Correctamente");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Funciones.mostrarMensaje(getApplicationContext(),"Ha ocurrido un error al cargar la imagen");
            }
        });
    }

    private void saveDataToFirebase(String item){
        //seteo atributos para el constructor
        String usuario = Funciones.obtenerUsuarioActivo(this).toUpperCase();
        String turno = Funciones.obtenerTurnoActual();
        String fechaHora = Funciones.obtenerFecha() + "_" + Funciones.obtenerHoraActual();

        //creacion del objeto registro
        RegistroDiario registro = new RegistroDiario(turno,item,fechaHora);

        //referencia a la raiz de mi bd
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("bdchecking");

        //estructura de coleccion
        DatabaseReference newRef = ref.child(Funciones.obtenerFecha()).child(usuario).push();

        //guarda registro en bd de firebase
        newRef.setValue(registro).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Funciones.mostrarMensaje(getApplicationContext(),"Registrado Correctamente");
                //Funciones.mostrarMensajeSnack(checkLayout,"Registrado Correctamente");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("TG","Ocurrio un error al guardar la data");
            }
        });
    }


}