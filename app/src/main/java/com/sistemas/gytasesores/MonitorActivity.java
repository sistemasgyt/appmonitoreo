package com.sistemas.gytasesores;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.codesgood.views.JustifiedTextView;

import CapaLogica.Funciones;
import CapaNegocio.Usuario;

public class MonitorActivity extends AppCompatActivity {

    Usuario usuario;

    TextView txtItems;
    TextView txtDetalleRegistro;
    JustifiedTextView txtIntroduccion;
    Button btnRegistrar;

    String textoIntroduccion,usuarioActivo;
    String itemsEvaluar = "Los items a evaluar serán los siguientes:\n\n" +
            "1.Manillar\n" +
            "2.Carenado\n" +
            "3.Espejos\n" +
            "4.Asientos\n" +
            "5.Llantas\n\n" +
            "Los datos del registro son los siguientes:";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor);

        //obteniendo datos enviados desde el LoginActivity
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle!=null){
            usuario = (Usuario) bundle.getSerializable("objUsuario");
            usuarioActivo = usuario.getIdUsuario().toUpperCase();
            /*nombres = usuario.getNombresCompletos();
            usuarioActual = usuario.getIdUsuario();
            idPersonal = usuario.getId();*/
        }

        //asignando variables
        txtItems = findViewById(R.id.txtItems);
        txtIntroduccion = findViewById(R.id.txtIntroduccion);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        txtDetalleRegistro = findViewById(R.id.txtDetalleRegistro);

        //obteniendo cadena de texto
        textoIntroduccion = getResources().getString(R.string.texto_introductorio);

        //actualizando componentes de texto
        txtIntroduccion.setText(Html.fromHtml(textoIntroduccion,Html.FROM_HTML_MODE_LEGACY));
        txtItems.setText(itemsEvaluar);

        String detalle = "Usuario: " + usuarioActivo + "\n" +
                "Fecha y Hora: " + Funciones.obtenerFecha() + " - " + Funciones.obtenerHoraActual() + "\n\n" +
                "Para iniciar, pulse en el botón REGISTRAR.";

        txtDetalleRegistro.setText(detalle);

        /*********************Verificando la habilitacion del boton registro*************************/
        Log.d("ULTIMA",Funciones.obtenerUltimaFechaRegistro(this));
        Log.d("HOY",Funciones.obtenerFecha());
        //primero se debe comparar la fecha actual con la que hay en memoria
        if(!Funciones.obtenerFecha().equals(Funciones.obtenerUltimaFechaRegistro(this))){
            Log.d("TAG","Las fechas son diferentes...Se inicializa las variables");
            Funciones.iniciarRegistroTurnos(this,Funciones.obtenerFecha());
        }else{// si las fechas son iguales, debemos validar si el usuario ha registrado el checklist en algun turno
            if(Funciones.obtenerTurnoActual().equals("M") && Funciones.obtenerRegistroTurno(this,"M")){
                btnRegistrar.setVisibility(View.GONE);
            }else if(Funciones.obtenerTurnoActual().equals("T") && Funciones.obtenerRegistroTurno(this,"T")){
                btnRegistrar.setVisibility(View.GONE);
            }else{
                btnRegistrar.setVisibility(View.VISIBLE);
            }
        }

        //evento asignado al boton registrar
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CheckActivity.class);
                startActivity(intent);
            }
        });
    }
}