package com.sistemas.gytasesores;

import android.app.Activity;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import com.sistemas.gytasesores.services.TrackingService;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import CapaLogica.Constantes;
import CapaLogica.Funciones;
import CapaNegocio.Usuario;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    Usuario usuario;
    NavigationView mNavigationView;
    View mHeaderView;
    TextView textViewUsername;
    TextView textNombres;
    int idPersonal;
    String usuarioActual;
    String nombres = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle!=null){
            usuario = (Usuario) bundle.getSerializable("objUsuario");
            nombres = usuario.getNombresCompletos();
            usuarioActual = usuario.getIdUsuario();
            idPersonal = usuario.getId();
        }
        // NavigationView
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        // NavigationView Header
        mHeaderView =  mNavigationView.getHeaderView(0);

        // View
        textViewUsername = (TextView) mHeaderView.findViewById(R.id.lblUsuarioMain);
        textNombres= (TextView) mHeaderView.findViewById(R.id.lblNombres);


        // Set username & email
        textViewUsername.setText(usuarioActual.toUpperCase());
        textNombres.setText(nombres);
        mNavigationView.setNavigationItemSelectedListener(this);

        Constantes.MAIN = this;

        //startTrackerService();
    }

    private void startTrackerService() {
        ContextCompat.startForegroundService(this, new Intent(this, TrackingService.class));
        //Notify the user that tracking has been enabled
        Funciones.mostrarMensaje(this,"GPS Monitoreo Habilitado");
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //noinspection SimplifiableIfStatement
        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putInt("IdUsuario",idPersonal);
        bundle.putString("UsuarioActual",usuarioActual);

         if(id == R.id.nav_salir){
            finish();
            Funciones.guardarEstadoSesion(getApplicationContext(),false,new Usuario("USER","ANONYMOUS",0));
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                startActivity(intent);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.checkLayout);
        fragment.onActivityResult(requestCode, resultCode, data);


    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
